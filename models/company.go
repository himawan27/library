package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type IndustryType int

const (
	IndustryTypeFAndB IndustryType = iota + 1
	IndustryTypeFinancial
	IndustryTypeFinancialNonBanks
	IndustryTypeTransportation
)

type OperationalDaysType int

const (
	OperationalDateFiveDays OperationalDaysType = iota + 1
	OperationalDateSixDays
)

//TODO
// What is this for????
type StatusRole int

const (
	StatusRoleHR StatusRole = iota + 1
	StatusRoleAdmin
	StatusRoleManager
	StatusRoleDirector
)

type CompanyStatus int

const (
	CompanyStatusActive CompanyStatus = iota + 1
	CompanyStatusNonActive
)

type Company struct {
	mgm.DefaultModel    `bson:",inline"`
	CompanyLogo         GlobalId            `json:"company_logo" bson:"company_logo"`
	CompanyName         string              `json:"company_name" bson:"company_name"`
	CompanyBrand        string              `json:"company_brand" bson:"company_brand"`
	CompanyPhoneNumber  string              `json:"company_phone_number" bson:"company_phone_number"`
	CompanyWebsite      string              `json:"company_website" bson:"company_website"`
	Industry            IndustryType        `json:"industry" bson:"industry"`
	CompanyAddress      AddressEntity       `json:"company_address" bson:"company_address"`
	NPWP                string              `json:"npwp" bson:"npwp"`
	OperationalCompany  OperationalCompany  `json:"operational_company" bson:"operational_company"`
	OperationalDaysType OperationalDaysType `json:"operational_days_type" bson:"operational_days_type"`
	HRName              string              `json:"hr_name" bson:"hr_name"`
	HRPhone             string              `json:"hr_phone" bson:"hr_phone"`
	HREmail             string              `json:"hr_email" bson:"hr_email"`
	StatusRole          GlobalId            `json:"status_role" bson:"status_role"`
	TotalUserActive     int64               `json:"total_user_active" bson:"total_user_active"`
	Status              CompanyStatus       `json:"status" bson:"status"`
	Currency            string              `json:"currency" bson:"currency"`
	Language            *Lang               `json:"language" bson:"language"`
	BasicSettings       bool                `json:"basic_settings" bson:"basic_settings"`
	Settings            CompanySettings     `json:"settings" bson:"settings"`
	DeletedAt           *time.Time          `json:"deleted_at" bson:"deleted_at"`
}

type SpecialKey int32

const (
	SpecialKeyMarriage SpecialKey = iota + 1
	SpecialKeyMarriageOfChildren
	SpecialKeyCircumciseChildren
	SpecialKeyBaptizeChildren
	SpecialKeyMaternityLeave
	SpecialKeyPaternityLeave
	SpecialKeyMiscarriageLeave
	SpecialKeyBereavementLeave
	SpecialKeyOtherFamilyBereavementLeave
)

type CompanySettings struct {
	WorkingHoursFix      CompanySettingsWHF   `json:"working_hours_fix" bson:"working_hours_fix"`
	WorkingHoursShifting CompanySettingsWHS   `json:"working_hours_shifting" bson:"working_hours_shifting"`
	WorkingHoursFlexible CompanySettingsWHFl  `json:"working_hours_flexible" bson:"working_hours_flexible"`
	EmploymentType       []GlobalId           `json:"employment_type" bson:"employment_type"`
	PayrollType          []GlobalId           `json:"payroll_type" bson:"payroll_type"`
	LateTolerance        float64              `json:"late_tolerance" bson:"late_tolerance"`
	Leave                CompanySettingsLeave `json:"leave" bson:"leave"`
}

type CompanySettingsLeave struct {
	Paid             CompanySettingsLPaid     `json:"paid" bson:"paid"`
	Sick             CompanySettingsSick      `json:"sick" bson:"sick"`
	Special          []CompanySettingsSpecial `json:"special" bson:"special"`
	PublicReducePaid bool                     `json:"public_reduce_paid" dson:"public_reduce_paid"`
}

type CompanySettingsSpecial struct {
	SpecialKey SpecialKey `json:"special_key" bson:"special_key"`
	Value      int        `json:"value" bson:"value"`
	Unit       string     `json:"unit" bson:"unit"`
}

type CompanySettingsLPaid struct {
	Max              int                           `json:"max" bson:"max"`
	IncrementPerYear int                           `json:"increment_per_year" bson:"increment_per_year"`
	Entitlement      CompanySettingsLPEntitlement  `json:"entitlement" bson:"entitlement"`
	CarryForward     CompanySettingsLPCarryForward `json:"carry_forward" bson:"carry_forward"`
}

type CompanySettingsLPEntitlement struct {
	Prorate            bool `json:"prorate" bson:"prorate"`
	EligibleAfterMonth int  `json:"eligible_after_month" bson:"eligible_after_month"`
}

type CompanySettingsLPCarryForward struct {
	Consent bool `json:"consent" bson:"consent"`
	Max     int  `json:"max" bson:"max"`
}

type CompanySettingsSick struct {
	Consent bool `json:"consent" bson:"consent"`
	Max     int  `json:"max" bson:"max"`
}

type CompanySettingsWHF struct {
	Consent   bool       `json:"consent" bson:"consent"`
	StartTime *time.Time `json:"start_time" bson:"start_time"`
	EndTime   *time.Time `json:"end_time" bson:"end_time"`
}

type CompanySettingsWHS struct {
	Consent bool `json:"consent" bson:"consent"`
}

type CompanySettingsWHFl struct {
	Consent bool `json:"consent" bson:"consent"`
}

type OperationalCompany struct {
	StartTime string `json:"start_time" bson:"start_time"`
	EndTime   string `json:"end_time" bson:"end_time"`
}

type Lang struct {
	Name string `json:"name" validate:"required"`
	Code string `json:"code" validate:"required"`
}

func (model *Company) CollectionName() string {
	return "company"
}

type PayrollType struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string     `json:"name" bson:"name"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *PayrollType) CollectionName() string {
	return "payroll_type"
}
