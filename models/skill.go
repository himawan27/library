package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Language struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Description      string     `json:"description" bson:"description"`
	Reading          string     `json:"reading" bson:"reading"`
	Writing          string     `json:"writing" bson:"writing"`
	Speaking         string     `json:"speaking" bson:"speaking"`
	Listening        string     `json:"listening" bson:"listening"`
}

func (model *Language) CollectionName() string {
	return "language"
}

type SoftSkill struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Description      string     `json:"description" bson:"description"`
}

func (model *SoftSkill) CollectionName() string {
	return "soft_skill"
}

type TechnicalSkill struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Description      string     `json:"description" bson:"description"`
}

func (model *TechnicalSkill) CollectionName() string {
	return "technical_skill"
}
