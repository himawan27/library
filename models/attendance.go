package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type AttendanceDay struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId    `json:"user" bson:"user"`
	Company          GlobalId    `json:"company" bson:"company"`
	Late             UnitType    `json:"late" bson:"late"`
	Overtime         UnitType    `json:"overtime" bson:"overtime"`
	WorkingHour      UnitType    `json:"working_hour" bson:"working_hour"`
	StampedInAt      time.Time   `json:"stamped_in_at" bson:"stamped_in_at"`
	StampedInMultiAt []time.Time `json:"stamped_in_multi_at" bson:"stamped_in_multi_at"`
	StampedOutAt     *time.Time  `json:"stamped_out_at" bson:"stamped_out_at"`
	DeletedAt        *time.Time  `json:"deleted_at" bson:"deleted_at"`
}

func (model *AttendanceDay) CollectionName() string {
	return "attendance_day"
}

type AttendanceMonth struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId   `json:"user" bson:"user"`
	Company          GlobalId   `json:"company" bson:"company"`
	MonthYear        string     `json:"month_year" bson:"month_year"`
	Late             UnitType   `json:"late" bson:"late"`
	Overtime         UnitType   `json:"overtime" bson:"overtime"`
	WorkingHour      UnitType   `json:"working_hour" bson:"working_hour"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *AttendanceMonth) CollectionName() string {
	return "attendance_month"
}

type ResultAvg struct {
	ID             *string `json:"id" bson:"id"`
	AvgLate        float64 `json:"avg_late" bson:"avg_late"`
	AvgOvertime    float64 `json:"avg_overtime" bson:"avg_overtime"`
	AvgWorkingHour float64 `json:"avg_working_hour" bson:"avg_working_hour"`
}

type Leave struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId   `json:"user" bson:"user"`
	Company          GlobalId   `json:"company" bson:"company"`
	LeaveType        GlobalId   `json:"leave_type" bson:"leave_type"`
	Reason           string     `json:"reason" bson:"reason"`
	StartDate        time.Time  `json:"start_date" bson:"start_date"`
	DueDate          time.Time  `json:"due_date" bson:"due_date"`
	Document         []GlobalId `json:"document" bson:"document"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *Leave) CollectionName() string {
	return "leave"
}
