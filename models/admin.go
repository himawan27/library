package models

import (
	"api-core/enums"
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"time"
)

type Admin struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time          `json:"deleted_at" bson:"deleted_at"`
	Password         string              `json:"password,omitempty" bson:"password"`
	Email            string              `json:"email" bson:"email"`
	FirstName        string              `json:"first_name" bson:"first_name"`
	LastName         string              `json:"last_name" bson:"last_name"`
	Status           enums.AdminStatus   `json:"status" bson:"status"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
}

func (model *Admin) CollectionName() string {
	return "admin"
}
