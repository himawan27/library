package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Organization struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	OrganizationName string     `json:"organization_name" bson:"organization_name"`
	Position         string     `json:"position" bson:"position"`
	Duration         string     `json:"duration" bson:"duration"`
}

func (model *Organization) CollectionName() string {
	return "organization"
}

type Achievement struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Description      string     `json:"description" bson:"description"`
	Level            string     `json:"level" bson:"level"`
	Duration         string     `json:"duration" bson:"duration"`
}

func (model *Achievement) CollectionName() string {
	return "achievement"
}
