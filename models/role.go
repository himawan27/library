package models

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Role struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string               `json:"name" bson:"name"`
	Company          GlobalId             `json:"company" bson:"company"`
	AccessRole       []primitive.ObjectID `json:"access_role" bson:"access_role"`
	CreatedBy        primitive.ObjectID   `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID   `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID  `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time           `json:"deleted_at" bson:"deleted_at"`
}

func (model *Role) CollectionName() string {
	return "roles"
}
