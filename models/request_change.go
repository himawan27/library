package models

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type RequestChange struct {
	mgm.DefaultModel `bson:",inline"`
	Category         GlobalId            `json:"category" bson:"category"`
	UserApproves     GlobalId            `json:"user_approves" bson:"user_approves"`
	Subject          string              `json:"subject" bson:"subject"`
	Description      string              `json:"description" bson:"description"`
	Status           Type                `json:"status" bson:"status"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"deleted_at" bson:"deleted_at"`
}

type RequestChangeVerbose struct {
	mgm.DefaultModel `bson:",inline"`
	Category         []ChangeRequestCategory `json:"category" bson:"category"`
	UserApproves     []User                  `json:"user_approves" bson:"user_approves"`
	Subject          string                  `json:"subject" bson:"subject"`
	Description      string                  `json:"description" bson:"description"`
	Status           Type                    `json:"status" bson:"status"`
	CreatedBy        primitive.ObjectID      `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID      `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID     `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time              `json:"deleted_at" bson:"deleted_at"`
}
