package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Family struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Salutation       string     `json:"salutation" bson:"salutation"`
	FirstName        string     `json:"first_name" bson:"first_name"`
	LastName         string     `json:"last_name" bson:"last_name"`
	Relationship     string     `json:"relationship" bson:"relationship"`
	Phone            string     `json:"phone" bson:"phone"`
	BloodType        string     `json:"blood_type" bson:"blood_type"`
	Birthday         time.Time  `json:"birthday" bson:"birthday"`
	Address          string     `json:"address" bson:"address"`
}

func (model *Family) CollectionName() string {
	return "family"
}
