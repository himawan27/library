package models

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BenefitType struct {
	mgm.DefaultModel `bson:",inline"`
	Company          GlobalId            `json:"company" bson:"company"`
	Type             string              `json:"type" bson:"type"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"-" bson:"deleted_at"`
}

func (model *BenefitType) CollectionName() string {
	return "benefit_type"
}
