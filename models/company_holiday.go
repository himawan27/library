package models

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CompanyHoliday struct {
	mgm.DefaultModel `bson:",inline"`
	Company          GlobalId            `json:"company" bson:"company"`
	Category         primitive.ObjectID  `json:"category" bson:"category"`
	Subject          string              `json:"subject" bson:"subject"`
	StartDate        time.Time           `json:"start_date" bson:"start_date"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	EndDate          time.Time           `json:"end_date" bson:"end_date"`
	DeletedAt        *time.Time          `json:"-" bson:"deleted_at"`
}

func (model *CompanyHoliday) CollectionName() string {
	return "company_holiday"
}
