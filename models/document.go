package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Document struct {
	mgm.DefaultModel `bson:",inline"`
	CloudPath        string     `json:"cloud_path" bson:"cloud_path"`
	OriginalName     string     `json:"original_name" bson:"original_name"`
	ContentType      string     `json:"content_type" bson:"content_type"`
	FileSize         int        `json:"file_size" bson:"file_size"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *Document) CollectionName() string {
	return "document"
}
