package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type FormalEducation struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	Document         []Document `json:"document" bson:"document"`
	Degree           string     `json:"degree" bson:"degree"`
	University       string     `json:"university" bson:"university"`
	Specialization   string     `json:"specialization" bson:"specialization"`
	City             string     `json:"city" bson:"city"`
	StartDate        time.Time  `json:"start_date" bson:"start_date"`
	EndDate          time.Time  `json:"end_date" bson:"end_date"`
	Gpa              int        `json:"gpa" bson:"gpa"`
	GpaMax           int        `json:"gpa_max" bson:"gpa_max"`
}

func (model *FormalEducation) CollectionName() string {
	return "formal_education"
}

type NonFormalEducation struct {
	mgm.DefaultModel     `bson:",inline"`
	DeletedAt            *time.Time `json:"deleted_at" bson:"deleted_at"`
	Document             []Document `json:"document" bson:"document"`
	NonFormalEducationId string     `json:"non_formal_education_id" bson:"non_formal_education_id"`
	NonFormalType        string     `json:"non_formal_type" bson:"non_formal_type"`
	University           string     `json:"university" bson:"university"`
	StudyOfField         string     `json:"study_of_field" bson:"study_of_field"`
	City                 string     `json:"city" bson:"city"`
	StartDate            time.Time  `json:"start_date" bson:"start_date"`
	EndDate              time.Time  `json:"end_date" bson:"end_date"`
	Score                int        `json:"score" bson:"score"`
}

func (model *NonFormalEducation) CollectionName() string {
	return "non_formal_education"
}
