package models

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Reimbursement struct {
	mgm.DefaultModel `bson:",inline"`
	ReimbursNumber   string              `json:"reimburs_number" bson:"reimburs_number"`
	Item             string              `json:"item" bson:"item"`
	ClaimAmmount     int64               `json:"claim_ammount" bson:"claim_ammount"`
	Receipt          *primitive.ObjectID `json:"receipt" bson:"receipt"`
	Status           *GlobalUnitType     `json:"status" bson:"status"`
	Requestby        string              `json:"request_by" bson:"request_by"`
	Company          GlobalId            `json:"company" bson:"company"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"deleted_at" bson:"deleted_at"`
}
