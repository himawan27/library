package models

import (
	"github.com/kamva/mgm/v3"
)

type Postal struct {
	mgm.DefaultModel `bson:",inline"`
	CityId           string `json:"city_id" bson:"city_id"`
	DistrictId       string `json:"district_id" bson:"district_id"`
	DistrictName     string `json:"district_name" bson:"district_name"`
	Name             string `json:"name" bson:"name"`
}
