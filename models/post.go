package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Post struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId   `json:"user" bson:"user"`
	Company          GlobalId   `json:"company" bson:"company"`
	Photo            []GlobalId `json:"photo" bson:"photo"`
	Video            []GlobalId `json:"video" bson:"video"`
	Description      string     `json:"description" bson:"description"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *Post) CollectionName() string {
	return "post"
}

type PostLike struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId   `json:"user" bson:"user"`
	Post             GlobalId   `json:"post" bson:"post"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *PostLike) CollectionName() string {
	return "post_like"
}
