package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type LeaveType struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string     `json:"name" bson:"name"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *LeaveType) CollectionName() string {
	return "leave_type"
}

type SpecialLeave struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string         `json:"name" bson:"name"`
	Day              GlobalUnitType `json:"day" bson:"day"`
	SpecialKey       SpecialKey     `json:"special_key" bson:"special_key"`
	DeletedAt        *time.Time     `json:"deleted_at" bson:"deleted_at"`
}

func (model *SpecialLeave) CollectionName() string {
	return "special_leave"
}

type IndonesianHoliday struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string     `json:"name" bson:"name"`
	StartDate        time.Time  `json:"start_date" bson:"start_date"`
	EndDate          time.Time  `json:"end_date" bson:"end_date"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *IndonesianHoliday) CollectionName() string {
	return "indonesian_holiday"
}
