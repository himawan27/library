package models

import (
	"github.com/kamva/mgm/v3"
)

type Province struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string `json:"name" bson:"name"`
}
