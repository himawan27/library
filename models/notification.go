package models

import (
	"encoding/json"
	"errors"
	"github.com/kamva/mgm/v3"
	"time"
)

type Notification struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId                  `json:"user" bson:"user"`
	Subject          string                    `json:"subject" bson:"subject"`
	RedirectPage     string                    `json:"redirect_page" bson:"redirect_page"`
	RedirectParam    NotificationRedirectParam `json:"redirect_param" bson:"redirect_param"`
	IsRead           bool                      `json:"is_read" bson:"is_read"`
	DeletedAt        *time.Time                `json:"deleted_at" bson:"deleted_at"`
}

type NotificationRedirectParam map[string]interface{}

func (nd *NotificationRedirectParam) ToJSON() ([]byte, error) {
	return json.Marshal(nd)
}

func (nd *NotificationRedirectParam) Scan(value interface{}) error {
	if value == nil {
		*nd = nil
		return nil
	}
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &nd)
}

func (model *Notification) CollectionName() string {
	return "notification"
}
