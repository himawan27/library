package models

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Shifting struct {
	mgm.DefaultModel `bson:",inline"`
	Company          GlobalId            `json:"company" bson:"company"`
	Name             string              `json:"name" bson:"name"`
	StartTime        string              `json:"start_time" bson:"start_time"`
	EndTime          string              `json:"end_time" bson:"end_time"`
	DeletedAt        *time.Time          `json:"deleted_at" bson:"deleted_at"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
}

func (model *Shifting) CollectionName() string {
	return "shifting"
}

type ShiftingSchedule struct {
	mgm.DefaultModel `bson:",inline"`
	Company          GlobalId            `json:"company" bson:"company"`
	User             GlobalId            `json:"user" bson:"user"`
	Shifting         GlobalId            `json:"shifting" bson:"shifting"`
	StartTime        time.Time           `json:"start_time" bson:"start_time"`
	EndTime          time.Time           `json:"end_time" bson:"end_time"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"deleted_at" bson:"deleted_at"`
}

func (model *ShiftingSchedule) CollectionName() string {
	return "shifting_schedule"
}
