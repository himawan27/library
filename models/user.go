package models

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type ReligionType int

const (
	ReligionIslam ReligionType = iota + 1
	ReligionProtestantism
	ReligionCatholicism
	ReligionHinduism
	ReligionBuddhism
	ReligionConfucianism
	ReligionTaoism
	ReligionAtheism
	ReligionOther
)

type User struct {
	mgm.DefaultModel  `bson:",inline"`
	Department        []GlobalId          `json:"department" bson:"department"`
	Company           GlobalId            `json:"company" bson:"company"`
	Password          string              `json:"password,omitempty" bson:"password"`
	Role              GlobalId            `json:"role" bson:"role"`
	Email             string              `json:"email" bson:"email"`
	Job               []GlobalId          `json:"job" bson:"job"`
	JoinDate          time.Time           `json:"join_date" bson:"join_date"`
	Status            Type                `json:"status" bson:"status"`
	BasicData         BasicData           `json:"basic_data" bson:"basic_data"`
	PersonalData      PersonalData        `json:"personal_data" bson:"personal_data"`
	EmergencyContact  EmergencyContact    `json:"emergency_contact" bson:"emergency_contact"`
	EmployeeData      EmployeeData        `json:"employee_data" bson:"employee_data"`
	AttendanceSetting AttendanceSetting   `json:"attendance_setting" bson:"attendance_setting"`
	AllowanceData     AllowanceData       `json:"allowance_data" bson:"allowance_data"`
	IsHr              bool                `json:"is_hr" bson:"is_hr"`
	AttendanceToday   AttendanceToday     `json:"attendance_today" bson:"attendance_today"`
	CreatedBy         primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy         primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy         *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	IsAdmin           bool                `json:"is_admin" bson:"is_admin"`
	DeletedAt         *time.Time          `json:"deleted_at" bson:"deleted_at"`
}

type BasicData struct {
	Photo       GlobalId `json:"photo" bson:"photo"`
	EmployeeID  string   `json:"employee_id" bson:"employee_id"`
	FirstName   string   `json:"first_name" bson:"first_name"`
	LastName    string   `json:"last_name" bson:"last_name"`
	PhoneNumber string   `json:"phone_number" bson:"phone_number"`
}

type PersonalData struct {
	Gender         Type          `json:"gender" bson:"gender"`
	MaritalStatus  Type          `json:"marital_status" bson:"marital_status"`
	Citizen        string        `json:"citizen" bson:"citizen"`
	IdentityNumber string        `json:"identity_number" bson:"identity_number"`
	Attachment     GlobalId      `json:"attachment" bson:"attachment"`
	PlaceOfBirth   string        `json:"place_of_birth" bson:"place_of_birth"`
	DateOfBirth    time.Time     `json:"date_of_birth" bson:"date_of_birth"`
	Religion       Type          `json:"religion" bson:"religion"`
	Address        AddressEntity `json:"address" bson:"address"`
	Education      []Education   `json:"education" bson:"education"`
	SocialMedia    SocialMedia   `json:"social_media" bson:"social_media"`
}

type AttendanceToday struct {
	IsWorking  bool      `json:"is_working" bson:"is_working"`
	Attendance *GlobalId `json:"attendance" bson:"attendance"`
}

type EmergencyContact struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
}

type EmployeeData struct {
	BankName           string    `json:"bank_name" bson:"bank_name"`
	BankAccountName    string    `json:"bank_account_name" bson:"bank_account_name"`
	BankAccountNumber  string    `json:"bank_account_number" bson:"bank_account_number"`
	BankBranch         string    `json:"bank_branch" bson:"bank_branch"`
	BPJSKTNumber       string    `json:"bpjskt_number" bson:"bpjskt_number"`
	BPJSKTActiveSince  time.Time `json:"bpjskt_active_since" bson:"bpjskt_active_since"`
	BPJSKESNumber      string    `json:"bpjskes_number" bson:"bpjskes_number"`
	BPJSKESActiveSince time.Time `json:"bpjskes_active_since" bson:"bpjskes_active_since"`
}

type SocialMedia struct {
	Facebook  string `json:"facebook" bson:"facebook"`
	Twitter   string `json:"twitter" bson:"twitter"`
	Telegram  string `json:"telegram" bson:"telegram"`
	Whatsapp  string `json:"whatsapp" bson:"whatsapp"`
	Instagram string `json:"instagram" bson:"instagram"`
}

type Education struct {
	NameOfSchool string `json:"name_of_school" bson:"name_of_school"`
	Major        string `json:"major" bson:"major"`
}

type AddressEntity struct {
	CountryName string `json:"country_name" bson:"country_name"`
	CountryCode string `json:"country_code" bson:"country_code"`
	StateName   string `json:"state_name" bson:"state_name"`
	StateCode   string `json:"state_code" bson:"state_code"`
	CityName    string `json:"city_name" bson:"city_name"`
	CityCode    string `json:"city_code" bson:"city_code"`
	Street      string `json:"street" bson:"street"`
	PostalCode  string `json:"postal_code" bson:"postal_code"`
}

// for FixWorkingHour, Sunday = 0
type AttendanceSetting struct {
	Type           Type                       `json:"type" bson:"type"`
	FixWorkingHour []AttendanceFixWorkingHour `json:"fix_working_hour" bson:"fix_working_hour"`
}

type AttendanceFixWorkingHour struct {
	StartTime string `json:"start_time" bson:"start_time"`
	EndTime   string `json:"end_time" bson:"end_time"`
	Day       uint8  `json:"day" bson:"day"`
}

type Type struct {
	Value int    `json:"value" bson:"value"`
	Text  string `json:"text" bson:"text"`
}

type AllowanceData struct {
	PayrollType       GlobalId       `json:"payroll_type" bson:"payroll_type"`
	Salary            float64        `json:"salary" bson:"salary"`
	FixAllowance      AllowanceType  `json:"fix_allowance" bson:"fix_allowance"`
	FlexibleAllowance AllowanceType  `json:"flexible_allowance" bson:"flexible_allowance"`
	BPJSTKJht         GlobalUnitType `json:"bpjstk_jht" bson:"bpjstk_jht"`
	BPJSTKJp          GlobalUnitType `json:"bpjstk_jp" bson:"bpjstk_jp"`
	BPJSKes           GlobalUnitType `json:"bpjs_kes" bson:"bpjs_kes"`
	BPJSTkKK          GlobalUnitType `json:"bpjs_tk_kk" bson:"bpjs_tk_kk"`
	NPWPNumber        string         `json:"npwp_number" bson:"npwp_number"`
	PTKP              Type           `json:"ptkp" bson:"ptkp"`
}

type AllowanceType struct {
	Value     float64           `json:"value"`
	Component []ComponentSalary `json:"component"`
}

type ComponentSalary struct {
	Title string  `json:"title"`
	Value float64 `json:"value"`
}

func (model *User) CollectionName() string {
	return "user"
}

type UserTypeAuth string

const (
	UserTypeAuthEmployee = "employee"
	UserTypeAuthHR       = "hr"
	UserTypeAuthAdmin    = "admin"
)

type AdminClaims struct {
	Id          string
	Email       string
	UserType    UserTypeAuth
	ApiVersion  string
	Environment string
	jwt.StandardClaims
}

type UserClaims struct {
	Id          string
	CompanyId   string
	UserType    UserTypeAuth
	Email       string
	ApiVersion  string
	Environment string
	jwt.StandardClaims
}

type EmployeeType struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string     `json:"name" bson:"name"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
}

func (model *EmployeeType) CollectionName() string {
	return "employee_type"
}

type UserVerbose struct {
	mgm.DefaultModel  `bson:",inline"`
	Name              string               `json:"name" bson:"name"`
	Department        []Department         `json:"department" bson:"department"`
	Company           []CompanyVerbose     `json:"company" bson:"company"`
	Password          string               `json:"password,omitempty" bson:"password"`
	Role              []Role               `json:"role" bson:"role"`
	Email             string               `json:"email" bson:"email"`
	Job               []Job                `json:"job" bson:"job"`
	JoinDate          time.Time            `json:"join_date" bson:"join_date"`
	Status            Type                 `json:"status" bson:"status"`
	BasicData         BasicDataVerbose     `json:"basic_data" bson:"basic_data"`
	PersonalData      PersonalDataVerbose  `json:"personal_data" bson:"personal_data"`
	EmergencyContact  EmergencyContact     `json:"emergency_contact" bson:"emergency_contact"`
	EmployeeData      EmployeeData         `json:"employee_data" bson:"employee_data"`
	AttendanceSetting AttendanceSetting    `json:"attendance_setting" bson:"attendance_setting"`
	AllowanceData     AllowanceDataVerbose `json:"allowance_data" bson:"allowance_data"`
	IsHr              bool                 `json:"is_hr" bson:"is_hr"`
	AttendanceToday   AttendanceToday      `json:"attendance_today" bson:"attendance_today"`
	DeletedAt         *time.Time           `json:"deleted_at" bson:"deleted_at"`
}

type BasicDataVerbose struct {
	Photo       []Document `json:"photo" bson:"photo"`
	EmployeeID  string     `json:"employee_id" bson:"employee_id"`
	FirstName   string     `json:"first_name" bson:"first_name"`
	LastName    string     `json:"last_name" bson:"last_name"`
	PhoneNumber string     `json:"phone_number" bson:"phone_number"`
}

type PersonalDataVerbose struct {
	Gender         Type          `json:"gender" bson:"gender"`
	MaritalStatus  Type          `json:"marital_status" bson:"marital_status"`
	Citizen        string        `json:"citizen" bson:"citizen"`
	IdentityNumber string        `json:"identity_number" bson:"identity_number"`
	Attachment     []Document    `json:"attachment" bson:"attachment"`
	PlaceOfBirth   string        `json:"place_of_birth" bson:"place_of_birth"`
	DateOfBirth    time.Time     `json:"date_of_birth" bson:"date_of_birth"`
	Religion       Type          `json:"religion" bson:"religion"`
	Address        AddressEntity `json:"address" bson:"address"`
	Education      []Education   `json:"education" bson:"education"`
	SocialMedia    SocialMedia   `json:"social_media" bson:"social_media"`
}

type AllowanceDataVerbose struct {
	PayrollType       []PayrollType  `json:"payroll_type" bson:"payroll_type"`
	Salary            float64        `json:"salary" bson:"salary"`
	FixAllowance      AllowanceType  `json:"fix_allowance" bson:"fix_allowance"`
	FlexibleAllowance AllowanceType  `json:"flexible_allowance" bson:"flexible_allowance"`
	BPJSTKJht         GlobalUnitType `json:"bpjstk_jht" bson:"bpjstk_jht"`
	BPJSTKJp          GlobalUnitType `json:"bpjstk_jp" bson:"bpjstk_jp"`
	BPJSKes           GlobalUnitType `json:"bpjs_kes" bson:"bpjs_kes"`
	BPJSTkKK          GlobalUnitType `json:"bpjs_tk_kk" bson:"bpjs_tk_kk"`
	NPWPNumber        string         `json:"npwp_number" bson:"npwp_number"`
	PTKP              Type           `json:"ptkp" bson:"ptkp"`
}

type CompanyVerbose struct {
	mgm.DefaultModel   `bson:",inline"`
	CompanyLogo        GlobalId `json:"company_logo" bson:"company_logo"`
	CompanyName        string   `json:"company_name" bson:"company_name"`
	CompanyBrand       string   `json:"company_brand" bson:"company_brand"`
	CompanyPhoneNumber string   `json:"company_phone_number" bson:"company_phone_number"`
	CompanyWebsite     string   `json:"company_website" bson:"company_website"`
}
