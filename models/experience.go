package models

import (
	"github.com/kamva/mgm/v3"
	"time"
)

type Experience struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        *time.Time `json:"deleted_at" bson:"deleted_at"`
	ExperienceId     string     `json:"experience_id" bson:"experience_id"`
	Position         string     `json:"position" bson:"position"`
	Company          string     `json:"company" bson:"company"`
	City             string     `json:"city" bson:"city"`
	StartDate        time.Time  `json:"start_date" bson:"start_date"`
	EndDate          time.Time  `json:"end_date" bson:"end_date"`
}

func (model *Experience) CollectionName() string {
	return "experience"
}
