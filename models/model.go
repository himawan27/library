package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type GlobalId struct {
	ID primitive.ObjectID `json:"id" bson:"_id,omitempty"`
}

type UnitType struct {
	Value float64 `json:"value" bson:"value"`
	Unit  string  `json:"unit" bson:"unit" default:"minute"`
}

type GlobalUnitType struct {
	Value float64 `json:"value" bson:"value"`
	Unit  string  `json:"unit" bson:"unit"`
}
