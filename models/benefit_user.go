package models

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BenefitUser struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId            `json:"user" bson:"user"`
	Company          GlobalId            `json:"company" bson:"company"`
	BenefitType      GlobalId            `json:"benefit_type" bson:"benefit_type"`
	AvailableAmount  int64               `json:"available_amount" bson:"available_amount"`
	BenefitAmount    int64               `json:"benefit_amount" bson:"benefit_amount"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"-" bson:"deleted_at"`
}

func (model *BenefitUser) CollectionName() string {
	return "benefit_user"
}

type BenefitUserVerbose struct {
	mgm.DefaultModel `bson:",inline"`
	User             GlobalId            `json:"user" bson:"user"`
	Company          []Company           `json:"company" bson:"company"`
	BenefitType      []BenefitType       `json:"benefit_type" bson:"benefit_type"`
	AvailableAmount  int64               `json:"available_amount" bson:"available_amount"`
	BenefitAmount    int64               `json:"benefit_amount" bson:"benefit_amount"`
	CreatedBy        primitive.ObjectID  `json:"created_by" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"updated_by" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"deleted_by" bson:"deleted_by"`
	DeletedAt        *time.Time          `json:"-" bson:"deleted_at"`
}
