package models

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CompanyHolidayCategory struct {
	mgm.DefaultModel `bson:",inline"`
	CategoryName     string              `json:"category_name" bson:"category_name"`
	Company          GlobalId            `json:"company" bson:"company"`
	DeletedAt        *time.Time          `json:"-" bson:"deleted_at"`
	CreatedBy        primitive.ObjectID  `json:"-" bson:"created_by"`
	UpdatedBy        primitive.ObjectID  `json:"-" bson:"updated_by"`
	DeletedBy        *primitive.ObjectID `json:"-" bson:"deleted_by"`
}

func (model *CompanyHolidayCategory) CollectionName() string {
	return "company_holiday_category"
}
