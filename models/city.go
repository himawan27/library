package models

import (
	"github.com/kamva/mgm/v3"
)

type City struct {
	mgm.DefaultModel `bson:",inline"`
	ProvinceId       string `json:"province_id" bson:"province_id"`
	Name             string `json:"name" bson:"name"`
}
